<?php
require 'src/view.php';

$view=new view();

$view->setPath('views/');//设置模板文件存放目录，必须以/结尾

//第一种设置输出参数的方法
$view->setAttr("title", "Variable example");
//第二种设置输出参数的方法
$view->array = array(
            '1' => "First array item",
            '2' => "Second array item",
            'n' => "N-th array item",
);
$view->j = 5;
//输入页头、内容、页脚
$view->display("header.php")->display('index.php')->display('footer.php')->render();

//直接输出单一文件

$view=new ('index.php');
$view->render();