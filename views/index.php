        <div class="left">
            <h2>Foreach &amp; if example</h2>
            {comment}
            这是注释
            {/comment}
            {if $array}
            {foreach $array as $key => $value}
            {$key} => {$value}<br />
            {/foreach}
            {else}
            {/*}这也是注释{*/}
            <p>Array is empty!</p>
            {/if}
        </div>
        <div class="left">    
            <h2>While example</h2>
            {$i = 1}
            {while $i < $j}
            显示 no. {$i}<br />
            {$i++}
            {/while}
        </div>